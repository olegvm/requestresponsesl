﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel.Description;
using System.Xml;

namespace MaryKay.ServiceLayer.Wcf
{
    public class CustomSerializerOperationBehavior : DataContractSerializerOperationBehavior
    {
        private Func<XmlObjectSerializer> _provider = () => null;

        public CustomSerializerOperationBehavior(OperationDescription description)
            : base(description) { }

        public override XmlObjectSerializer CreateSerializer(Type type, string name, string ns, IList<Type> knownTypes)
        {
            return _provider();
        }

        public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
        {
            return _provider();
        }

        public CustomSerializerOperationBehavior With(Func<XmlObjectSerializer> provider)
        {
            _provider = provider;
            return this;
        }
    }
}