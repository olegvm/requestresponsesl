﻿using System;
using System.ServiceModel;

namespace MaryKay.ServiceLayer.Wcf
{
    public static class ClientProxyExtensions
    {
        public static ClientBase<T> WithTimeouts<T>(this ClientBase<T> client, params TimeSpan[] timeouts) 
            where T : class
        {
            client.Endpoint.Binding.OpenTimeout = timeouts[0];
            client.Endpoint.Binding.ReceiveTimeout = (timeouts.Length > 1) ? timeouts[1] : timeouts[0];
            client.Endpoint.Binding.SendTimeout = (timeouts.Length > 2) ? timeouts[2] : timeouts[0];
            client.Endpoint.Binding.CloseTimeout = (timeouts.Length > 3) ? timeouts[3] : timeouts[0];

            return client;
        }
    }
}