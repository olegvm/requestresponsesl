﻿using System.ServiceModel;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Wcf
{
    [ServiceContract(Namespace = "urn:marykay-servicelayer-messageservice")]
    [CustomSerializer]
    public interface IMessageService
    {
        [OperationContract(AsyncPattern = true)]
        Task<IMessage> ProcessMessageAsync(IMessage message);
    }
}