﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Wcf
{
    public class MessageServiceBootstrapper : BootstrapperBase
    {
        public override void RegisterServices(IContainer container)
        {
            container.Register<IMessageService>(c => new MessageService())
                     .Register<ILoggerFacade>(c => new LoggerBase());
        }

        public override void RegisterHandlersChain(IHandlersChainBuilder builder)
        {
            builder.Add(new LoggingHandler())
                   .Add(new ExceptionHandlingHandler())
                   .Add(new NullRequestShortCircuitHandler())
                   .Add(new CompositeMessageHandler())
                   .Add(new RequestProcessingHandler());
        }

        public override void RegisterProcessors(IContainer container)
        {
            container.Register<IMessageProcessor>(c => new MessageProcessorBase());
        }

        public override void RegisterHandlers(IContainer container)
        {
            var types = from asm in AppDomain.CurrentDomain.GetAssemblies()
                                .Where(a => a.FullName.StartsWith("MaryKay."))
                        from type in asm.GetTypesSafe()
                        where !type.IsAbstract
                               && !type.IsInterface
                               && typeof(IHandlingService).IsAssignableFrom(type)
                               && type.GetConstructor(new Type[] { }) != null
                        select type;

            foreach (var type in types)
            {
                var instance = Expression.Constant(Activator.CreateInstance(type));
                var methods = type.GetMethods(BindingFlags.Public
                                                | BindingFlags.Instance
                                                | BindingFlags.FlattenHierarchy)
                                   .Where(m => m.Name.StartsWith("Handle"));
                foreach (var mi in methods)
                {
                    var ptype = mi.GetParameters()[0].ParameterType;
                    var key = ptype.FullName;

                    var param = Expression.Parameter(typeof(object));
                    var handler = Expression.Lambda<Func<object, Task<object>>>(
                                        Expression.Call(instance, mi, Expression.Convert(param, ptype))
                                       ,param).Compile();

                    container.RegisterProvider(typeof(Func<object, Task<object>>), key, _ => handler);
                }
            }
        }
    }
}