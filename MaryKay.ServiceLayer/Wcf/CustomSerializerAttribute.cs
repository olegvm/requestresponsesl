﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace MaryKay.ServiceLayer.Wcf
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Interface)]
    public class CustomSerializerAttribute : Attribute, IOperationBehavior, IServiceBehavior, IContractBehavior
    {
        public CustomSerializerAttribute()
        {
            Provider = () => new NetDataContractSerializer();
        }

        public CustomSerializerAttribute(Func<XmlObjectSerializer> provider)
        {
            Provider = provider;
        }

        public Func<XmlObjectSerializer> Provider { get; private set; }

        void IOperationBehavior.AddBindingParameters(
            OperationDescription description,
            BindingParameterCollection parameters)
        {
        }

        void IOperationBehavior.ApplyClientBehavior(
            OperationDescription description,
            ClientOperation proxy)
        {
            ReplaceDataContractSerializerOperationBehavior(description);
        }

        void IOperationBehavior.ApplyDispatchBehavior(
            OperationDescription description,
            DispatchOperation dispatch)
        {
            ReplaceDataContractSerializerOperationBehavior(description);
        }

        void IOperationBehavior.Validate(OperationDescription description)
        {
        }

        void IServiceBehavior.AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
            ReplaceDataContractSerializerOperationBehavior(serviceDescription);
        }

        void IServiceBehavior.ApplyDispatchBehavior(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
        {
            ReplaceDataContractSerializerOperationBehavior(serviceDescription);
        }

        void IServiceBehavior.Validate(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
        {
        }

        void IContractBehavior.AddBindingParameters(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint,
            BindingParameterCollection bindingParameters)
        {
        }

        void IContractBehavior.ApplyClientBehavior(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint,
            ClientRuntime clientRuntime)
        {
            ReplaceDataContractSerializerOperationBehavior(contractDescription);
        }

        void IContractBehavior.ApplyDispatchBehavior(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint,
            DispatchRuntime dispatchRuntime)
        {
            ReplaceDataContractSerializerOperationBehavior(contractDescription);
        }

        void IContractBehavior.Validate(
            ContractDescription contractDescription,
            ServiceEndpoint endpoint) { }

        private void ReplaceDataContractSerializerOperationBehavior(ServiceDescription description)
        {
            foreach (var endpoint in description.Endpoints)
            {
                ReplaceDataContractSerializerOperationBehavior(endpoint);
            }
        }

        private void ReplaceDataContractSerializerOperationBehavior(ContractDescription description)
        {
            foreach (var operation in description.Operations)
            {
                ReplaceDataContractSerializerOperationBehavior(operation);
            }
        }

        private void ReplaceDataContractSerializerOperationBehavior(ServiceEndpoint endpoint)
        {
            // ignore mex
            if (endpoint.Contract.ContractType == typeof(IMetadataExchange))
            {
                return;
            }
            ReplaceDataContractSerializerOperationBehavior(endpoint.Contract);
        }

        private void ReplaceDataContractSerializerOperationBehavior(OperationDescription description)
        {
            var behavior = description.Behaviors.Find<DataContractSerializerOperationBehavior>();
            if (behavior != null)
            {
                description.Behaviors.Remove(behavior);
                var newBehavior = new CustomSerializerOperationBehavior(description)
                    .With(Provider);
                description.Behaviors.Add(newBehavior);
            }
        }
    }
}