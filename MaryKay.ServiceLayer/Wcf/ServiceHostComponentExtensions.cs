﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace MaryKay.ServiceLayer.Wcf
{
    public static class ServiceHostComponentExtensions
    {
        public static ServiceHostComponent<I, T> ForService<I, T>(this ServiceHostComponent<I, T> component, params Uri[] baseAddresses)
        {
            component.ServiceHost = new System.ServiceModel.ServiceHost(typeof(T), baseAddresses);
            return component;
        }

        public static ServiceHostComponent<I, T> WithEndpoint<I, T>(this ServiceHostComponent<I, T> component, Binding binding, string address = "")
        {
            component.ServiceHost.AddServiceEndpoint(typeof(I), binding, address);
            return component;
        }

        public static ServiceHostComponent<I, T> WithNetTcpEndpoint<I, T>(this ServiceHostComponent<I, T> component, string address = "")
        {
            return component.WithEndpoint(new NetTcpBinding(), address);
        }

        public static ServiceHostComponent<I, T> WithHttpMexEndpoint<I, T>(this ServiceHostComponent<I, T> component, string address = "mex")
        {
            var smb = new ServiceMetadataBehavior();
            smb.HttpGetEnabled = true;
            smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            component.ServiceHost.Description.Behaviors.Add(smb);
            component.ServiceHost.AddServiceEndpoint(typeof(IMetadataExchange), new BasicHttpBinding(), address);

            return component;
        }

        public static ServiceHostComponent<I, T> IncludeExceptionDetailInFaults<I, T>(this ServiceHostComponent<I, T> component, bool flag = false)
        {
            var sdb = (ServiceDebugBehavior)component.ServiceHost.Description.Behaviors[typeof(ServiceDebugBehavior)];
            sdb.IncludeExceptionDetailInFaults = flag;

            return component;
        }

        public static ServiceHostComponent<I, T> WithTimeouts<I, T>(this ServiceHostComponent<I, T> component, params TimeSpan[] timeouts)
        {
            if (timeouts.Length == 0)
                return component;

            foreach (var endpoint in component.ServiceHost.Description.Endpoints)
            {
                if (endpoint.GetType() != typeof(IMetadataExchange))
                {
                    endpoint.Binding.OpenTimeout = timeouts[0];
                    endpoint.Binding.ReceiveTimeout = (timeouts.Length > 1) ? timeouts[1] : timeouts[0];
                    endpoint.Binding.SendTimeout = (timeouts.Length > 2) ? timeouts[2] : timeouts[0];
                    endpoint.Binding.CloseTimeout = (timeouts.Length > 3) ? timeouts[3] : timeouts[0];
                }
            }

            return component;
        }

    }
}