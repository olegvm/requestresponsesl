﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Wcf
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple
                    , InstanceContextMode = InstanceContextMode.PerCall)]
    public class MessageService : IMessageService
    {
        static MessageService()
        {
            var bs_count = Bootstrapper.Count; // <- all heavy work inside
        }

        #region IMessageService Members

        public async Task<IMessage> ProcessMessageAsync(IMessage message)
        {
            try
            {
                var processor = IoC.Resolve<IMessageProcessor>();
                return await processor.ProcessAsync(message);
            }
            catch (Exception ex)
            {
                return message.Failed(ex);
            }
        }

        #endregion
    }
}