﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Wcf
{
    public class MessageServiceClientProxy : IMessageProcessor
    {
        private Binding _binding;
        private EndpointAddress _endpointAddress;

        #region .ctor

        public MessageServiceClientProxy(Binding binding, EndpointAddress remoteAddress)
        {
            _binding = binding;
            _endpointAddress = remoteAddress;
        }

        #endregion

        #region IMessageProcessor Members

        public IMessage Process(IMessage message)
        {
            return ProcessAsync(message).Result;
        }

        public async Task<IMessage> ProcessAsync(IMessage message)
        {
            IMessageService channel = null;
            try
            {
                channel = ChannelFactory<IMessageService>.CreateChannel(_binding, _endpointAddress);
                return await channel.ProcessMessageAsync(message)
                                    .ContinueWith(t =>
                                        {
                                            var tcs = new TaskCompletionSource<IMessage>();
                                            var error = t.Result.As<Exception>();
                                            if (error != null)
                                            {
                                                var ex =
                                                    new ApplicationException(
                                                        "Server side exception, see InnerException for details.", error);
                                                tcs.SetException(ex);
                                                return tcs.Task;
                                            }
                                            return t;
                                        }).Unwrap();
            }
            finally
            {
                Dispose(channel as ICommunicationObject);
            }
        }

        public void Dispose() { }

        private void Dispose(ICommunicationObject channel)
        {
            if(channel == null)
                return;

            try
            {
                channel.Close();
            }
            catch (Exception)
            {
                channel.Abort();
                throw;
            }
            finally
            {
                channel = null;
            }
        }

        #endregion
    }
}