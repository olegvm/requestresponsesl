﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Sys = System.ServiceModel;

namespace MaryKay.ServiceLayer.Wcf
{
    public class ServiceHostComponent<I, T> : IDisposable
    {

        private ServiceHostComponent() { }

        public static ServiceHostComponent<I, T> Default
        {
            get { return new ServiceHostComponent<I, T>(); }
        }

        public Sys.ServiceHost ServiceHost { get; set; }

        public string BaseAddresses
        {
            get { return string.Join(Environment.NewLine, ServiceHost.BaseAddresses); }
        }

        private EventWaitHandle _event = new ManualResetEvent(false);
        private Task _task;
        public void Start()
        {
            _task = Task.Factory.StartNew((o) =>
            {
                var evt = (EventWaitHandle)o;
                do
                {
                    ServiceHost.Open();
                } while (!evt.WaitOne());
                ServiceHost.Close();
            }
            , _event, TaskCreationOptions.LongRunning);
        }

        public void Stop()
        {
            try
            {
                _event.Set();
                _task.Wait();
            }
            catch (AggregateException ex)
            {
                throw ex.InnerException;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.Stop();
        }

        #endregion
    }
}