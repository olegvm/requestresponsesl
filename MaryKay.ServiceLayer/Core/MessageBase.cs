﻿using System;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Core
{
    public abstract class MessageBase : IMessage
    {
        protected MessageBase() { } // <- just to avoid direct creation derived types in external code.

        #region IMessage Members

        public Guid ID { get; set; }
        public MessageState State { get; set; }
        public virtual object Body { get; set; }

        #endregion

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as IMessage;
            if (other != null)
                return this.ID == other.ID;
            return false;
        }

        public override string ToString()
        {
            return this.ToJsonString((o) => o.ID, (o) => o.State, (o) => o.Body);
        }
    }

    public abstract class MessageBase<T, TMessage> : MessageBase
        where TMessage : class, IMessage, new()
    {
        public static TMessage New
        {
            get { return new TMessage(); }
        }

        public static TMessage Default
        {
            get { return New.WithID(Guid.NewGuid()); }
        }
    }

    // a couple of helpers to shortcut usage in code
    public class Request<T> : MessageBase<T, Request<T>> { }

    public static class Request
    {
        public static IMessage For<T>(T body)
        {
            return Request<T>.Default.WithBody(body);
        }
    }

    public class Response<T> : MessageBase<T, Response<T>> { }
}