﻿using System.Collections.Generic;

namespace MaryKay.ServiceLayer.Core
{
    public class CompositeMessage : MessageBase<IEnumerable<IMessage>, CompositeMessage>, ICollection<IMessage>
    {
        private IList<IMessage> _list = new List<IMessage>();

        public override object Body
        {
            get { return _list; }
            set { _list = (IList<IMessage>)value; }
        }

        public IEnumerable<IMessage> Items 
        {
            get { return _list; }
        }

        #region ICollection<IMessage> Members

        public void Add(IMessage item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(IMessage item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(IMessage[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(IMessage item)
        {
            return _list.Remove(item);
        }

        #endregion

        #region IEnumerable<IMessage> Members

        public IEnumerator<IMessage> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}