﻿using System;
using System.Threading.Tasks;

namespace MaryKay.ServiceLayer.Core
{
    public interface IMessageProcessor : IDisposable
    {
        IMessage Process(IMessage message);
        Task<IMessage> ProcessAsync(IMessage message);
    }
}