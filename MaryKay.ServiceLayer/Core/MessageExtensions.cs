﻿using System;
using System.Collections.Generic;

namespace MaryKay.ServiceLayer.Core
{
    public static class Message
    {
        public static bool IsFailed(this IMessage message)
        {
            return message.State == MessageState.Failed && message.Body is Exception;
        }

        public static bool IsComposite(this IMessage message)
        {
            return message.Body is IEnumerable<IMessage>;
        }

        public static T As<T>(this IMessage message)
        {
            return (T)message.Body;
        }

        public static TMessage Processed<T, TMessage>(this TMessage message, T body)
            where TMessage : IMessage
        {
            return message.WithState(MessageState.Processed).WithBody(body);
        }

        public static TMessage Rejected<T, TMessage>(this TMessage message, T body)
            where TMessage : IMessage
        {
            return message.WithState(MessageState.Rejected).WithBody(body);
        }

        public static TMessage Failed<TMessage>(this TMessage message, Exception ex)
            where TMessage : IMessage
        {
            return message.WithState(MessageState.Failed).WithBody(ex);
        }

        public static TMessage WithID<TMessage>(this TMessage message, Guid guid) 
            where TMessage : IMessage
        {
            message.ID = guid;
            return message;
        }

        public static TMessage WithState<TMessage>(this TMessage message, MessageState state)
            where TMessage : IMessage
        {
            message.State = state;
            return message;
        }

        public static TMessage WithBody<T, TMessage>(this TMessage message, T body)
            where TMessage : IMessage
        {
            message.Body = body;
            return message;
        }
    }
}