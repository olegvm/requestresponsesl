﻿using System;

namespace MaryKay.ServiceLayer.Core
{
    public interface IMessage
    {
        Guid ID { get; set; }
        MessageState State { get; set; }
        object Body { get; set; }
    }

    public enum MessageState
    {
        New,
        Sending,
        Sent,
        Receiving,
        Received,
        Processing,
        Processed,
        Rejected,
        Failed,
        Default = New,
    }
}