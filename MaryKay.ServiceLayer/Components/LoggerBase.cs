﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public class LoggerBase : ILoggerFacade
    {
        private Func<string, Task> _writerAction = s => 
        { 
            Debug.WriteLine(s);
            return Task.FromResult<object>(null);
        };
        private Func<string, Task> _writeSilent = s => Task.FromResult<object>(null);
        private Func<string, Task> _writeError = s => Task.FromResult<object>(null);
        private Func<string, Task> _writeVerbose = s => Task.FromResult<object>(null);
        private Func<string, Task> _writeDebug = s => Task.FromResult<object>(null);
            
        #region ILoggerFacade Members

        public ILoggerFacade SetVerbosityLevel(VerbosityLevel level)
        {
            // a little state machine to not bother with multilevel if-s in logging handler
            
            switch (level)
            {
                case VerbosityLevel.Error:
                {
                    _writeError = _writerAction;
                    _writeVerbose = _writeSilent;
                    _writeDebug = _writeSilent;
                    break;
                }
                case VerbosityLevel.Verbose:
                {
                    _writeError = _writerAction;
                    _writeVerbose = _writerAction;
                    _writeDebug = _writeSilent;
                    break;
                }
                case VerbosityLevel.Debug:
                {
                    _writeError = _writerAction;
                    _writeVerbose = _writerAction;
                    _writeDebug = _writerAction;
                    break;
                }
                case VerbosityLevel.Silent:
                {
                    _writeError = _writeSilent;
                    _writeVerbose = _writeSilent;
                    _writeDebug = _writeSilent;
                    break;
                }
                default:
                {
                    _writeError = _writerAction;
                    _writeVerbose = _writeSilent;
                    _writeDebug = _writeSilent;
                    break;
                }
            }

            return this;
        }

        public ILoggerFacade SetWriterAction(Func<string, Task> writerAction)
        {
            _writerAction = writerAction;

            return this;
        }

        public virtual async Task WriteDebugAsync(string format, params object[] data)
        {
            await _writeDebug(string.Format(format, data));
        }

        public virtual async Task WriteVerboseAsync(string format, params object[] data)
        {
            await _writeVerbose(string.Format(format, data));
        }

        public virtual async Task WriteErrorAsync(string format, params object[] data)
        {
            await _writeError(string.Format(format, data));
        }

        #endregion
    }
}