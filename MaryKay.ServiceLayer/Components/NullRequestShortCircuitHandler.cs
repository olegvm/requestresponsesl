﻿using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public class NullRequestShortCircuitHandler : MessageHandlerBase
    {
        public override async Task<IMessage> HandleAsync(IMessage request)
        {
            if (request.Body == null)
            {
                // send back to client (kind of shortcircuit to quick testing without exceptions).
                return request.Rejected("Unable to process a [null] request.");
            }

            return await Next(request);
        }
    }
}