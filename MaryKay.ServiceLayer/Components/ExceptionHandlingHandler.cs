﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public class ExceptionHandlingHandler : MessageHandlerBase
    {
        public override async Task<IMessage> HandleAsync(IMessage request)
        {
            try
            {
                return await Next(request);
            }
            catch (Exception ex)
            {
                return request.Failed(ex);
            }
        }
    }
}