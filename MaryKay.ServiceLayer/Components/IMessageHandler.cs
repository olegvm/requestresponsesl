﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public interface IMessageHandler
    {
        Func<IMessage, Task<IMessage>> Next { get; set; }
        Task<IMessage> HandleAsync(IMessage request);
    }
}