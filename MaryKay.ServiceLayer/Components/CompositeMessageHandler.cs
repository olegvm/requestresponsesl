﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public class CompositeMessageHandler : MessageHandlerBase
    {
        public override async Task<IMessage> HandleAsync(IMessage request)
        {
            if (!request.IsComposite())
                return await Next(request);

            var list = request.As<IEnumerable<IMessage>>();
            var handler = IoC.Resolve<Func<IMessage, Task<IMessage>>>();
            var results = await Task.WhenAll(list.Select(handler).ToArray());

            return request.Processed(results);
        }
    }
}