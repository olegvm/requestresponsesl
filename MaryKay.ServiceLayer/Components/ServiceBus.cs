﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public static class ServiceBus
    {
        public static IMessageProcessor InContextOf(string key)
        {
            var processor = IoC.Resolve<IMessageProcessor>(key);

            return processor;
        }

        public static Task<IMessage> ForRequest<T>(this IMessageProcessor processor, T instance)
        {
            var request = Request<T>.Default.WithBody(instance);
            return processor.ProcessAsync(request);
        }

        public static Task<T> OnResponseOf<T>(this Task<IMessage> task, Action<T> action)
            where T : class
        {
            return task.ContinuationTaskSync<IMessage, T>(t =>
            {
                var result = t.Result.As<T>();
                action(result);
                return Task.FromResult(result);
            });
        }

        public static Task<T> Then<T>(this Task<T> task, Action<T> action)
        {
            return task.ContinueSync<T>(action);
        }

        public static Task<AsyncVoid> Catch<T, E>(this Task<T> task, Action<E> handler) where E : Exception
        {
            return task.ContinuationTaskSync<T, AsyncVoid>(t =>
            {
                // don't use TaskContinuationOptions.OnlyOnFaulted here 
                // since it prevents the subsequent Finally<T>() block to run on successfull branch.
                if (t.IsFaulted && t.Exception != null)
                {
                    t.Exception.Handle(ex =>
                    {
                        if (ex is AggregateException)
                        {
                            ex = ex.InnerException; // unwrap if required.
                        }
                        if (!(ex is E))
                            throw ex; // propagate unmatched exceptions to the next or external catch block.

                        handler((E)ex);
                        return true;
                    });
                }
                return t.CompletedVoid();
            });
        }

        public static Task<T> Finally<T>(this Task<T> task, Action handler)
        {
            return task.ContinueSync<T>(handler);
        }

        internal static Task<T> ContinueSync<T>(this Task<T> task, Action action)
        {
            return task.ContinuationTaskSync<T, T>(t => { action(); return t; });
        }

        internal static Task<T> ContinueSync<T>(this Task<T> task, Action<T> action)
        {
            return task.ContinuationTaskSync<T, T>(t => { action(t.Result); return t; });
        }

        internal static Task<T> ContinueSync<T>(this Task<T> task, Action<Task<T>> action)
        {
            return task.ContinuationTaskSync<T, T>(t => { action(t); return t; });
        }

        internal static Task<TOut> ContinuationTaskSync<TIn, TOut>(this Task<TIn> task, Func<Task<TIn>, Task<TOut>> func, TaskScheduler taskScheduler = null)
        {
            var options = TaskContinuationOptions.ExecuteSynchronously;
            var token = CancellationToken.None;
            var scheduler = taskScheduler ?? TaskScheduler.Current;

            var tcs = new TaskCompletionSource<Task<TOut>>();
            task.ContinueWith(t =>
            {
                try
                {
                    tcs.TrySetResult(func(t));
                }
                catch (Exception ex)
                {
                    tcs.TrySetException(ex);
                }

                return default(TOut);
            }, token, options, scheduler);

            return tcs.Task.Unwrap();
        }

        private static Func<Task<AsyncVoid>> _getCompleted = () =>
        {
            var tcs = new TaskCompletionSource<AsyncVoid>();
            tcs.SetResult(default(AsyncVoid));
            _getCompleted = () => tcs.Task;

            return _getCompleted();
        };

        public static Task<AsyncVoid> CompletedVoid(this Task task)
        {
            return _getCompleted();
        }

        public static Task<T> FromException<T>(this Task task, Exception ex)
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetException(ex);
            return tcs.Task;
        }
    }

    public struct AsyncVoid { }
}