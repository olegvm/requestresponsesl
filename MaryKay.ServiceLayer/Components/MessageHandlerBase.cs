﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public abstract class MessageHandlerBase : IMessageHandler
    {
        public MessageHandlerBase()
        {
            Next = Task.FromResult;
        }

        #region IMessageHandler Members

        public Func<IMessage, Task<IMessage>> Next { get; set; }

        public abstract Task<IMessage> HandleAsync(IMessage request);

        #endregion
    }
}