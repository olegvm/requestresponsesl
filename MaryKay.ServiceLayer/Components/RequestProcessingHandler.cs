﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public class RequestProcessingHandler : MessageHandlerBase
    {
        public override async Task<IMessage> HandleAsync(IMessage request)
        {
            var handler = IoC.Resolve<Func<object, Task<object>>>(request.Body.GetType().FullName);
            var result = await handler(request.Body);

            return request.Processed(result);
        }
    }
}