﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public class DefaultBuilder : IHandlersChainBuilder
    {
        public Stack<IMessageHandler> Handlers { get; private set; }

        public DefaultBuilder()
        {
            Handlers = new Stack<IMessageHandler>();
        }

        #region IHandlersChainBuilder Members

        public IHandlersChainBuilder Add(IMessageHandler handler)
        {
            if (Handlers.Any())
            {
                var prev = Handlers.Peek();
                prev.Next = handler.HandleAsync;
            }

            Handlers.Push(handler);
            
            return this;
        }

        public Func<IMessage, Task<IMessage>> Build()
        {
            var chain = Handlers.Reverse().First();
            return chain.HandleAsync;
        }

        #endregion
    }
}