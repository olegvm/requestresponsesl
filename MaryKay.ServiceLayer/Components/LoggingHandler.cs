﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public class LoggingHandler : MessageHandlerBase
    {
        private readonly ILoggerFacade _logger = IoC.Resolve<ILoggerFacade>();

        public override async Task<IMessage> HandleAsync(IMessage request)
        {
            await _logger.WriteVerboseAsync("Request {{{0}}} received with state '{1}' and data of type:[{2}].", request.ID, request.State, request.Body == null ? "null" : request.Body.GetType().FullName);

            await _logger.WriteDebugAsync("Request Body contains: [{0}].", request.Body == null ? "null" : request.Body.ToString());

            var response = await Next(request);

            if (response.State == MessageState.Failed)
                await _logger.WriteErrorAsync(response.As<Exception>().ToSmoothString());

            await _logger.WriteVerboseAsync("Response {{{0}}} sent with state '{1}' and data of type:[{2}].", response.ID, response.State, response.Body == null ? "null" : response.Body.GetType().FullName);

            return response;
        }
    }
}