﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Components
{
    public interface IHandlersChainBuilder
    {
        IHandlersChainBuilder Add(IMessageHandler handler);
        Func<IMessage, Task<IMessage>> Build();
    }
}