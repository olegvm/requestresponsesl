﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Components
{
    public class MessageProcessorBase : IMessageProcessor, IDisposable
    {
        #region IMessageProcessor Members

        public virtual IMessage Process(IMessage request)
        {
            return ProcessAsync(request).Result;
        }

        public virtual async Task<IMessage> ProcessAsync(IMessage request)
        {
            var handler = IoC.Resolve<Func<IMessage,Task<IMessage>>>();
            var response = await handler(request);

            return response;
        }

        public virtual void Dispose() { }

        #endregion
    }
}