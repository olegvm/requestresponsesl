﻿using System;

namespace MaryKay.ServiceLayer.Infrastructure
{
    using ProvFunc = Func<IContainer, object>;

    public static class ContainerExtensions
    {
        public static IContainer Register<I>(this IContainer container, ProvFunc provider, string context = null)
        {
            return container.RegisterProvider(typeof(I), context, provider);
        }

        public static IContainer Register<I, T>(this IContainer container, ProvFunc provider)
        {
            return container.RegisterProvider(typeof(I), typeof(T).FullName, provider);
        }

        public static I Resolve<I>(this IContainer container, string context = null, bool throwOnUnresolved = true)
        {
            return (I)container.Resolve(typeof(I), context, throwOnUnresolved);
        }

        public static I Resolve<I, T>(this IContainer container, bool throwOnUnresolved = true)
        {
            return (I)container.Resolve(typeof(I), typeof(T).FullName, throwOnUnresolved);
        }

    }
}