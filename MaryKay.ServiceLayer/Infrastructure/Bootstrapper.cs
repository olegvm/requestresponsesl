﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public static class Bootstrapper
    {
        static Bootstrapper()
        {
            Bootstrap();
        }

        public static int Count = 0;

        private static void Bootstrap()
        {
            var loaded = AppDomain.CurrentDomain.GetAssemblies()
                                    .Where(a => a.FullName.StartsWith("MaryKay."))
                                    .Select(a => a.Location).ToList();
            var root = Path.GetDirectoryName(typeof(IBootstrapper).Assembly.Location);
            foreach (var file in Directory.GetFiles(root, "MaryKay.*.dll")
                                            .Where(f => !loaded.Contains(f)))
            {
                Assembly.LoadFile(file);
            }

            var types = (from asm in AppDomain.CurrentDomain.GetAssemblies()
                         where asm.FullName.StartsWith("MaryKay.")
                         from type in asm.GetTypesSafe()
                         where !type.IsAbstract
                                && !type.IsInterface
                                && typeof(IBootstrapper).IsAssignableFrom(type)
                                && type.GetConstructor(new Type[] { }) != null
                         select type
                ).ToList();

            foreach (var type in types)
            {
                var bs = (IBootstrapper)Activator.CreateInstance(type);
                bs.Run();
            }

            Count++; // <- for smooth testing
        }
    }
}