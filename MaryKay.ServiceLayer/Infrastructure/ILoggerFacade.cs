﻿using System;
using System.Threading.Tasks;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public interface ILoggerFacade
    {
        ILoggerFacade SetVerbosityLevel(VerbosityLevel level);
        ILoggerFacade SetWriterAction(Func<string, Task> writerAction);
        Task WriteDebugAsync(string format, params object[] data);
        Task WriteVerboseAsync(string format, params object[] data);
        Task WriteErrorAsync(string format, params object[] data);
    }

    public enum VerbosityLevel
    {
        Error,
        Verbose,
        Debug,
        Silent,
        Default = Error
    }
}