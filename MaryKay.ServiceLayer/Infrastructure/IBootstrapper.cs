﻿namespace MaryKay.ServiceLayer.Infrastructure
{
    public interface IBootstrapper
    {
        void ConfigureContainer(IContainer container);
        void Run();
    }
}