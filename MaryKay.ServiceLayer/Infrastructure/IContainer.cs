﻿using System;

namespace MaryKay.ServiceLayer.Infrastructure
{
    // own abstraction of IoC container to protect variations from several IoC implementations.
    // (Unity, Spring, Castle, etc.)
    public interface IContainer : IDisposable
    {
        IContainer Parent { get; set; }
        IContainer RegisterProvider(Type type, string context, Func<IContainer, object> provider);
        object Resolve(Type type, string context, bool throwOnUnresolved);
        bool TryResolve(Type type, string context, out object instance);
    }
}