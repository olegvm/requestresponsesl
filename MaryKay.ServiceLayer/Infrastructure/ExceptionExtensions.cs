﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public static class ExceptionExtensions
    {
        public static string ToSmoothString(this Exception ex)
        {
            Func<Exception, string> format = x =>
                                            string.Concat(x.GetType().FullName, ": "
                                                        , x.Message, Environment.NewLine
                                                        , x.StackTrace, Environment.NewLine);
            var stack = new Stack<string>();
            stack.Push(format(ex));

            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
                stack.Push(string.Concat("---------   Inner Exception Details   ---------", Environment.NewLine
                                         , "InnerException: ", format(ex)));
            }

            return string.Concat(stack.Reverse());
        }
         
    }
}