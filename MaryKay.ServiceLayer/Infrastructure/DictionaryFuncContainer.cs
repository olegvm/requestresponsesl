﻿using System;
using System.Collections.Generic;

namespace MaryKay.ServiceLayer.Infrastructure
{
    using ContextKey = Tuple<Type, string>;
    using ProvFunc = Func<IContainer, object>;

    public class DictionaryFuncContainer : IContainer, IDisposable
    {
        private readonly IDictionary<ContextKey, ProvFunc> _providers = new Dictionary<ContextKey, ProvFunc>();

        public DictionaryFuncContainer()
        {
            // just to make a container resolve itself
            ProvFunc func = (c) => this;
            _providers[new ContextKey(typeof(IContainer), null)] = func;
        }

        #region IContainer Members

        public IContainer Parent { get; set; }

        public IContainer RegisterProvider(Type type, string context, ProvFunc provider)
        {
            _providers[new ContextKey(type, context)] = provider;

            return this;
        }

        public object Resolve(Type type, string context, bool throwOnUnresolved)
        {
            object instance;
            if (!TryResolve(type, context, out instance) && throwOnUnresolved)
            {
                throw new ApplicationException(string.Format("Can't resolve '{0}' implementation "
                    + "in the context of '{1}'.", type.FullName, context ?? "null"));
            }

            return instance;
        }

        public bool TryResolve(Type type, string context, out object instance)
        {
            instance = null;
            ProvFunc provider;
            if (this.TryResolveProvider(type, context, out provider))
            {
                instance = provider(this);
                return true;
            }

            if (this.Parent != null && this.Parent.TryResolve(type, context, out instance))
            {
                return true;
            }

            // give them a chance with default .ctor injection
            if (!type.IsAbstract && !type.IsInterface && !type.IsGenericType)
            {
                var closure = instance = Activator.CreateInstance(type);
                _providers[new ContextKey(type, null)] = c => closure;
                return true;
            }

            return false;
        }

        #endregion

        #region IDisposable Members

        public virtual void Dispose()
        {
            foreach (var provider in _providers.Values)
            {
                var disposable = provider(this) as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
        }

        #endregion

        private bool TryResolveProvider(Type type, string context, out ProvFunc provider)
        {
            return _providers.TryGetValue(new ContextKey(type, context), out provider);
        }
    }
}