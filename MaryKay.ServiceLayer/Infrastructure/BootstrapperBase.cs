﻿using System;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public abstract class BootstrapperBase : IBootstrapper
    {
        private IContainer _container;

        public BootstrapperBase()
            : this(IoC.Resolve<IContainer>())
        {
            Run();
        }

        public BootstrapperBase(IContainer container)
        {
            _container = container;
        }

        #region IBootstrapper Members

        public virtual void ConfigureContainer(IContainer container)
        {
            RegisterServices(container);

            var builder = new DefaultBuilder();
            container.Register<IHandlersChainBuilder>(c => builder);
            RegisterHandlersChain(builder);
            var handlersChain = builder.Build();
            container.Register<Func<IMessage, Task<IMessage>>>(c => handlersChain);

            RegisterProcessors(container);
            RegisterHandlers(container);
        }

        public void Run()
        {
            ConfigureContainer(_container);
        }

        #endregion

        public abstract void RegisterHandlersChain(IHandlersChainBuilder builder);

        public abstract void RegisterServices(IContainer container);

        public abstract void RegisterProcessors(IContainer container);

        public abstract void RegisterHandlers(IContainer container);
    }
}