﻿using System;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public static class IoC
    {
        private static Func<IContainer> _provider;
        private static IContainer _container;

        static IoC()
        {
            _provider = () => new DictionaryFuncContainer();
            _container = _provider();
        }

        public static IContainer InjectProvider(Func<IContainer> provider)
        {
            lock (_container)
            {
                _provider = provider;
                _container = _provider();
            }
            return _container;
        }

        public static IContainer AttachContainer()
        {
            var container = _provider();
            container.Parent = _container;
            lock (_container)
            {
                _container = container;
            }
            return _container;
        }

        public static void DetachContainer(IContainer container)
        {
            lock (_container)
            {
                _container = container.Parent;
            }
            container.Dispose();
            container = null;
        }

        public static void RegisterInstance<T>(T instance, string context = null)
        {
            _container.RegisterProvider(typeof(T), context, _ => instance);
        }

        public static void RegisterType<I, T>(string context = null) where T : I, new() 
        {
            _container.RegisterProvider(typeof(I), context, _ => new T());
        }

        public static T Resolve<T>(string context = null, bool throwOnUnresolved = true)
        {
            return (T)_container.Resolve(typeof(T), context, throwOnUnresolved);
        }

        public static T TryResolve<T>(string context = null)
        {
            return Resolve<T>(context, false);
        }
    }
}