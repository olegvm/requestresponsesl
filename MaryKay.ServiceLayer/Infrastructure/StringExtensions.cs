﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public static class StringExtensions
    {
         public static string ToJsonString<T>(this T instance, params Expression<Func<T,object>>[] getters) where T : class
         {
             if (instance == null)
                 return "d:null";
             var sb = new StringBuilder();

             var props = getters.Select(gt => typeof (T).GetProperty((gt.Body as MemberExpression).Member.Name));

             foreach (var pi in props)
             {
                 sb.AppendFormat("{0}:'{1}',", pi.Name, pi.GetValue(instance));
             }

             return string.Format("d:{{{0}}}", sb.ToString());
         }
    }
}