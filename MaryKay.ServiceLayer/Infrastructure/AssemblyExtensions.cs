﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MaryKay.ServiceLayer.Infrastructure
{
    public static class AssemblyExtensions
    {
        // for details see here: http://stackoverflow.com/questions/7889228/how-to-prevent-reflectiontypeloadexception-when-calling-assembly-gettypes
        public static IEnumerable<Type> GetTypesSafe(this Assembly assembly)
        {
            if (assembly == null)
                throw new ArgumentNullException("assembly");
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException ex)
            {
                return ex.Types.Where(t => t != null);
            }
        }
    }
}