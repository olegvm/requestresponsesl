﻿using System;
using System.Collections.Generic;
using MaryKay.CRC.Contracts.Entities.Imports;
using MaryKay.CRC.Contracts.ServiceContracts.Imports;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;
using MaryKay.ServiceLayer.Services.ServiceAdapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MaryKay.ServiceLayer.Tests.Service
{
    [TestClass]
    public class ImportServiceAdaptersTests
    {
        public TestContext TestContext { get; set; }

        private static IContainer _testContextContainer;
        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            var bs = Bootstrapper.Count; // <- initialize all registrations

            var mock = new Mock<IImportService>();
            mock.Setup(svc => svc.GetImportCategory(It.IsAny<short>(), It.IsAny<int>()))
                                 .Returns(new ImportCategory { ImportCategoryID = 42, ShortDescription = "mock"});
            mock.Setup(svc => svc.GetImportType(It.IsAny<short>(), It.IsAny<int>()))
                                 .Returns(new ImportType { ImportTypeID = 42, ShortDescription = "mock"});
            mock.Setup(svc => svc.IsReconcile(It.IsAny<short>(), It.IsAny<DateTime>(), It.IsAny<int>()))
                                 .Returns(42);
            mock.Setup(svc => svc.GetImportCategories(It.IsAny<int>()))
                                 .Returns(new List<ImportCategory> { new ImportCategory { ImportCategoryID = 42, ShortDescription = "mock" }, new ImportCategory { ImportCategoryID = 43, ShortDescription = "mock1" } });
            mock.Setup(svc => svc.GetImportTypes(It.IsAny<int>(), It.IsAny<short>()))
                                 .Returns(new List<ImportType> { new ImportType { ImportTypeID = 42, ShortDescription = "mock" }, new ImportType { ImportTypeID = 43, ShortDescription = "mock1" } });
            mock.Setup(svc => svc.CAMMaxMonthsAllowed(It.IsAny<int>()))
                                 .Returns(42);
            mock.Setup(svc => svc.ValidateImportFile(It.IsAny<ImportFile>()))
                                 .Returns(new List<string> { "mock", "mock1" });
            mock.Setup(svc => svc.GetEnvServerPath(It.IsAny<int>()))
                                 .Returns("mock");
            mock.Setup(svc => svc.ProcessImportFile(It.IsAny<ImportFile>()))
                                 .Returns(new List<ImportMessage> { new ImportMessage { FieldName = "mock", Message = "mock", SequenceNumber = 42 }, new ImportMessage { FieldName = "mock1", Message = "mock1", SequenceNumber = 43 } });

            _testContextContainer = IoC.AttachContainer();
            _testContextContainer.Register<IImportService>(_ => mock.Object)
                                 .Register<IMessageProcessor>(_ => new MessageProcessorBase(), "EUR1");
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            IoC.DetachContainer(_testContextContainer);
        }

        [TestMethod]
        public void _01_CreateImportServiceClientAdapterInstance()
        {
            var client = new ImportServiceClientAdapter();
            Assert.IsNotNull(client);
            Assert.IsInstanceOfType(client, typeof(IImportService));
        }

        [TestMethod]
        public void _02_GetImportCategory()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.GetImportCategory(42, 34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ImportCategory));
            Assert.IsTrue(result.ImportCategoryID == 42, "result.ImportCategoryID == 42");
            Assert.IsTrue(result.ShortDescription == "mock", "result.ShortDescription == 'mock'");
        }

        [TestMethod]
        public void _03_GetImportType()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.GetImportType(42, 34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ImportType));
            Assert.IsTrue(result.ImportTypeID == 42, "result.ImportTypeID == 42");
            Assert.IsTrue(result.ShortDescription == "mock", "result.ShortDescription == 'mock'");
        }

        [TestMethod]
        public void _04_IsReconcile()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.IsReconcile(42, DateTime.Now, 34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(int));
            Assert.IsTrue(result == 42, "result == 42");
        }

        [TestMethod]
        public void _05_GetImportCategories()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.GetImportCategories(34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<ImportCategory>));
            Assert.IsTrue(result.Count == 2, "result.Count == 2");
            Assert.IsTrue(result[0].ImportCategoryID == 42, "result[0].ImportCategoryID == 42");
            Assert.IsTrue(result[0].ShortDescription == "mock", "result[0].ShortDescription == 'mock'");
            Assert.IsTrue(result[1].ImportCategoryID == 43, "result[1].ImportCategoryID == 43");
            Assert.IsTrue(result[1].ShortDescription == "mock1", "result[1].ShortDescription == 'mock1'");
        }

        [TestMethod]
        public void _06_GetImportTypes()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.GetImportTypes(34, 42);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<ImportType>));
            Assert.IsTrue(result.Count == 2, "result.Count == 2");
            Assert.IsTrue(result[0].ImportTypeID == 42, "result[0].ImportTypeID == 42");
            Assert.IsTrue(result[0].ShortDescription == "mock", "result[0].ShortDescription == 'mock'");
            Assert.IsTrue(result[1].ImportTypeID == 43, "result[1].ImportTypeID == 43");
            Assert.IsTrue(result[1].ShortDescription == "mock1", "result[1].ShortDescription == 'mock1'");
        }

        [TestMethod]
        public void _07_CAMMaxMonthsAllowed()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.CAMMaxMonthsAllowed(34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(int));
            Assert.IsTrue(result == 42, "result == 42");
        }

        [TestMethod]
        public void _08_ValidateImportFile()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.ValidateImportFile(new ImportFile { SubsidiaryID = 34 });

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<string>));
            Assert.IsTrue(result.Count == 2, "result.Count == 2");
            Assert.IsTrue(result[0] == "mock", "result[0] == 'mock'");
            Assert.IsTrue(result[1] == "mock1", "result[1] == 'mock1'");
        }

        [TestMethod]
        public void _09_GetEnvServerPath()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.GetEnvServerPath(34);

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(string));
            Assert.IsTrue(result == "mock", "result == 'mock'");
        }

        [TestMethod]
        public void _10_ProcessImportFile()
        {
            var client = new ImportServiceClientAdapter();
            var result = client.ProcessImportFile(new ImportFile { SubsidiaryID = 34 });

            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(List<ImportMessage>));
            Assert.IsTrue(result.Count == 2, "result.Count == 2");
            Assert.IsTrue(result[0].SequenceNumber == 42, "result[0].SequenceNumber == 42");
            Assert.IsTrue(result[0].FieldName == "mock", "result[0].FieldName == 'mock'");
            Assert.IsTrue(result[0].Message == "mock", "result[0].Message == 'mock'");
            Assert.IsTrue(result[1].SequenceNumber == 43, "result[1].SequenceNumber == 43");
            Assert.IsTrue(result[1].FieldName == "mock1", "result[1].FieldName == 'mock1'");
            Assert.IsTrue(result[1].Message == "mock1", "result[1].Message == 'mock1'");
        }
    }
}
