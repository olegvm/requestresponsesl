﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;
using MaryKay.ServiceLayer.Wcf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MessageState = MaryKay.ServiceLayer.Core.MessageState;

namespace MaryKay.ServiceLayer.Tests.Service
{
    [TestClass]
    public class MessageServiceTests
    {
        private static readonly bool UseExternalServiceHost = false;
        // IMPORTANT: run the following cmd in admin mode to enable 80 port in UAC mode (Win 7|8):
        // D:\>netsh http add urlacl url=http://+:80/ServiceLayer/MessageService user=DOMAIN\user
        // (net.tcp:// schema doesn't require this step)
        private static Uri nettcpBaseAddress = UseExternalServiceHost
                            ? new Uri("net.tcp://localhost/WCF/EUR1/CRCServices/Services/MessageService.svc")
                            : new Uri("net.tcp://localhost/ServiceLayer/MessageService");

        private static ServiceHostComponent<IMessageService, MessageService> _host;
        private static Action _runOnce = (() =>
        {
            if (!UseExternalServiceHost)
                Thread.Sleep(3000);
            _runOnce = () => { };
        });

        private static readonly Binding _netBinding = new NetTcpBinding(SecurityMode.None);
        [ClassInitialize]
        public static void HostTestsInitialize(TestContext testContext)
        {
            var bs_count = Bootstrapper.Count;

            if (!UseExternalServiceHost)
            {
                var httpBaseAddress = new Uri("http://localhost/ServiceLayer/MessageService");

                _host = ServiceHostComponent<IMessageService, MessageService>.Default
                                                                .ForService(nettcpBaseAddress,
                                                                            httpBaseAddress)
                                                                .WithEndpoint(_netBinding)
                                                                .WithHttpMexEndpoint()
                                                                .IncludeExceptionDetailInFaults(true);
#if DEBUG
                _host.WithTimeouts(TimeSpan.FromHours(1));
#endif

                _host.Start();
            }
        }

        [ClassCleanup]
        public static void HostTestsCleanup()
        {
            if (!UseExternalServiceHost)
                _host.Stop();
        }

        [TestInitialize]
        public void LetTheHostStartProperly()
        {
            _runOnce();
        }

        public TestContext TestContext { get; set; }

        [TestMethod]
        [WorkItem(165592)]
        public void _01_IContainerCanResolveItself()
        {
            var actual = IoC.Resolve<IContainer>();
            var redundant = IoC.Resolve<IContainer>();

            Assert.IsNotNull(actual);
            Assert.IsNotNull(redundant);
            Assert.IsInstanceOfType(actual, typeof(IContainer));
            Assert.IsInstanceOfType(redundant, typeof(IContainer));
            Assert.AreSame(actual, redundant);
            Assert.IsTrue(actual == redundant, "actual == redundant");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _02_BootstrapperCanInitializeAllDependencies()
        {
            var service = new MessageService(); // <- it makes a bootstrapper to start
            service = new MessageService(); // <- to check if the bootstrapper doesn't initialized twice

            var container = IoC.Resolve<IContainer>();

            Assert.IsNotNull(IoC.Resolve<IMessageService>());
            Assert.IsNotNull(IoC.Resolve<IMessageProcessor>());
            //Assert.IsNotNull(IoC.Resolve<Func<object, object>, CAPImportRequest>());
            //Assert.IsNotNull(IoC.Resolve<Func<object, object>, UPPImportOptionsRequest>());

            Assert.IsInstanceOfType(IoC.Resolve<IMessageService>(), typeof(MessageService));
            Assert.IsInstanceOfType(IoC.Resolve<IMessageProcessor>(), typeof(MessageProcessorBase));
            Assert.IsTrue(Bootstrapper.Count == 1, "Bootstrapper.Count == 1"); // <- after several calls of new MessageService(); 
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _08_ClientProxyCreatedBySyncClientFactoryWithoutRunnigServer()
        {
            try
            {
                var client = ChannelFactory<IMessageService>.CreateChannel(_netBinding, new EndpointAddress("net.tcp://somewhere"));

                Assert.IsNotNull(client);
            }
            catch (Exception ex)
            {
                TestContext.WriteLine(ex.ToString());
                Assert.Fail(ex.Message);
            }

        }

        [TestMethod]
        [WorkItem(165592)]
        public void _09_ClientProxyCreatedByDefaultCtorWithoutRunnigServer()
        {
            try
            {
                using (var client = new MessageServiceClientProxy(_netBinding, new EndpointAddress("net.tcp://somewhere")))
                {
                    Assert.IsNotNull(client);
                }
            }
            catch (Exception ex)
            {
                TestContext.WriteLine(ex.ToString());
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _09_ClientProxyCanReceiveRejectedMessageOnNullRequest()
        {
            try
            {
                using (var client = new MessageServiceClientProxy(_netBinding, new EndpointAddress(nettcpBaseAddress)))
                {
                    var response = client.Process(Request<string>.New);

                    Assert.IsNotNull(response);
                    Assert.IsTrue(response.ID == Guid.Empty, "response.ID == Guid.Empty");
                    Assert.IsTrue(response.State == MessageState.Rejected, "response.State == MessageState.Rejected");
                    Assert.IsTrue(response.As<string>() == "Unable to process a [null] request.");
                }
            }
            catch (Exception ex)
            {
                var info = ex.ToSmoothString();
                TestContext.WriteLine(info);
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _10_ClientProxyCanReceiveExceptionMessageFromRunnigServer()
        {
            using (var client = new MessageServiceClientProxy(_netBinding, new EndpointAddress(nettcpBaseAddress)))
            {
                var request = Request<string>.New.WithBody("mock");
                Assert.IsTrue(request.ID == Guid.Empty, "request.ID == Guid.Empty");
                Assert.IsTrue(request.State == MessageState.New, "request.State == MessageState.New");
                Assert.IsTrue(request.As<string>() == "mock", "request.Body == 'mock'");

                bool caught = false;
                IMessage response = null;
                try
                {
                    response = client.Process(request);
                }
                catch (Exception ex)
                {
                    TestContext.WriteLine(ex.ToSmoothString());
                    caught = true;
                    Assert.IsNull(response); // <- client throws on receiving and incapsulate the Failed message into an AggregateException.
                        
                    var actual = ex.InnerException;
                    Assert.IsNotNull(actual);
                    Assert.IsTrue(actual.Message == "Server side exception, see InnerException for details.", "actual.Message is not matched");
                    var inner = actual.InnerException;
                    Assert.IsNotNull(inner);
                    Assert.IsInstanceOfType(inner, typeof(ApplicationException));
                    Assert.IsTrue(inner.Message == string.Format("Can't resolve '{0}' implementation in the context of '{1}'.", typeof(Func<object,Task<object>>).FullName, typeof(string).FullName), "inner.Message is not matched");
                }

                Assert.IsTrue(caught, "caught");
            }
        }
    }
}
