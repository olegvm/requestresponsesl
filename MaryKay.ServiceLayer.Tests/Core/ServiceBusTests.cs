﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MaryKay.ServiceLayer.Tests.Core
{
    [TestClass]
    public class ServiceBusTests
    {
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void InitializeContainer(TestContext context)
        {
            var bs_count = Bootstrapper.Count;
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _01_ContainerCanResolveInstanceInSubsidiaryContext()
        {
            var list = new Dictionary<string, string>
                {
                    {"RU", "EUR1"},
                    {"CZ", "EUR2"},
                    {"ES", "EUR3"},
                    {"AU", "AP1"},
                };
            foreach (var pair in list)
            {
                var processor = GetProcessor(pair.Key, pair.Value);

                Assert.IsNotNull(processor, string.Format("processor:{0}\\{1}", pair.Key, pair.Value));
                Assert.IsInstanceOfType(processor, typeof(IMessageProcessor), string.Format("typeof(IMessageProcessor):{0}\\{1}", pair.Key, pair.Value));
            }
        }


        [TestMethod]
        [WorkItem(165592)]
        public void _02_ContainerThrowsOnUnableToResolve()
        {
            try
            {
                GetProcessor("RU", "EUR2");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ApplicationException), "typeof(ApplicationException)");
                Assert.IsTrue(ex.Message.StartsWith(string.Format("Can't resolve '{0}' implementation "
                    + "in the context of '{1}'.", typeof(IMessageProcessor).FullName, "EUR1")), "ex.Message not matched");
            }
        }


        [TestMethod]
        [WorkItem(165592)]
        public void _03_RequestForCanReturnAndUnwrapMockedTask()
        {
            var request = "request";
            var task = GetProcessor("RU", "EUR1").ForRequest(request);

            Assert.IsNotNull(task, "task != null");
            TestContext.WriteLine(task.GetType().Name);
            Assert.IsInstanceOfType(task, typeof(Task<IMessage>), "typeof(Task<IMessage>)");

            var message = task.Result.As<string>();

            Assert.IsNotNull(message, "message != null");
            Assert.IsInstanceOfType(message, typeof(string), "typeof(string)");
            Assert.IsTrue(message == "mock", "message == 'mock'");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _04_OnResponseOfCanHandleMockedResponse()
        {
            var request = "request";
            var response = "";
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                 .OnResponseOf<string>(s => response = s);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<string>), "typeof(Task<string>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _05_OnResponseOfIsFaultedWhenForRequestThrowsFirst()
        {
            var request = Guid.Empty;
            var response = "";
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                     .OnResponseOf<string>(s => response = s);
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (AggregateException ex)
            {
                Assert.IsInstanceOfType(ex, typeof(AggregateException));
                Assert.IsInstanceOfType(ex.Flatten().InnerException, typeof(ArgumentNullException));
                Assert.IsTrue((ex.Flatten().InnerException as ArgumentNullException).ParamName == "mock", "ex.InnerException.ParamName == \"mock\"");

                Assert.IsNotNull(task, "task != null");
                Assert.IsInstanceOfType(task, typeof(Task<string>), "typeof(Task<string>)");
                Assert.IsTrue(task.Status == TaskStatus.Faulted, "task.Status == TaskStatus.Faulted");

                Assert.IsNotNull(response, "response != null");
                Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
                Assert.IsTrue(response == "", "response == ''");
                return;
            }

            Assert.Fail("ArgumentNullException's not been caught.");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _06_OnResponseOfThrowsWhenHandlerLambdaThrowsFirst()
        {
            var request = "request";
            var response = "";
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                });
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(AggregateException));
                Assert.IsInstanceOfType(ex.InnerException, typeof(DivideByZeroException));
                Assert.IsTrue((ex.InnerException as DivideByZeroException).Message == "hello from handler", "ex.InnerException.Message == \"hello from handler\"");

                Assert.IsNotNull(task, "task != null");
                Assert.IsInstanceOfType(task, typeof(Task<string>), "typeof(Task<string>)");
                Assert.IsTrue(task.Status == TaskStatus.Faulted, "task.Status == TaskStatus.Faulted");

                Assert.IsNotNull(response, "response != null");
                Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
                Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set earlier that the exception has been thrown.
                return;
            }

            Assert.Fail("DivideByZeroException's not been caught.");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _07_CatchHandlesWhenForRequestThrowsFirst()
        {
            var request = Guid.Empty;
            var response = "";
            ArgumentNullException ex = null;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                })
                                                .Catch((ArgumentNullException x) => ex = x);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.IsTrue(ex.ParamName == "mock", "ex.ParamName == \"mock\"");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "", "response == ''"); // <- set should not be performed.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _08_CatchHandlesMostCommonExceptionWhenHandlerLambdaThrowsFirst()
        {
            var request = "request";
            var response = "";
            Exception ex = null;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                })
                                                .Catch((Exception x) => ex = x);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsInstanceOfType(ex, typeof(DivideByZeroException));
            Assert.IsTrue(ex.Message == "hello from handler", "ex.ParamName == \"hello from handler\"");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _09_CatchDoesNotHandleSpecifiedExceptionWhenHandlerLambdaThrowsFirst()
        {
            var request = "request";
            var response = "";
            Exception ex = null;
            var caught = false;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                })
                                                .Catch((ArgumentNullException x) => ex = x);
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (AggregateException dex)
            {
                caught = true;
                Assert.IsInstanceOfType(dex.InnerException, typeof(DivideByZeroException));
                Assert.IsTrue(dex.InnerException.Message == "hello from handler");
            }

            Assert.IsNull(ex, "ex");
            Assert.IsTrue(caught, "caught");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.Faulted, "task.Status == TaskStatus.Faulted");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _10_CatchHandlesSuitableExceptionWhenHandlerLambdaThrowsFirst()
        {
            var request = "request";
            var response = "";
            Exception anex = null;
            Exception dbzex = null;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                })
                                                .Catch((ArgumentNullException x) => anex = x)
                                                .Catch((Exception x) => dbzex = x);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsNull(anex, "anex");
            Assert.IsInstanceOfType(dbzex, typeof(DivideByZeroException));
            Assert.IsTrue(dbzex.Message == "hello from handler", "ex.ParamName == \"hello from handler\"");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _11_FinallyRunsOnSuccesfulResponse()
        {
            var request = "request";
            var response = "";
            Exception anex = null;
            Exception dbzex = null;
            var finalized = false;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                })
                                                .Catch((ArgumentNullException x) => anex = x)
                                                .Catch((Exception x) => dbzex = x)
                                                .Finally(() => finalized = true);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsNull(anex, "anex");
            Assert.IsNull(dbzex, "dbzex");

            Assert.IsTrue(finalized, "finalized");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed without throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _12_FinallyRunsOnFaultResponse()
        {
            var request = Guid.Empty;
            var response = "";
            ArgumentNullException anex = null;
            Exception dbzex = null;
            var finalized = false;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                })
                                                .Catch((DivideByZeroException x) => dbzex = x)
                                                .Catch((ArgumentNullException x) => anex = x)
                                                .Finally(() => finalized = true);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsNotNull(anex, "anex");
            Assert.IsTrue(anex.ParamName == "mock", "anex.ParamName == \"mock\"");
            Assert.IsNull(dbzex, "dbzex");

            Assert.IsTrue(finalized, "finalized");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "", "response == ''"); // <- set should not be performed.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _13_FinallyRunsOnFaultResponseHandler()
        {
            var request = "request";
            var response = "";
            ArgumentNullException anex = null;
            Exception dbzex = null;
            var finalized = false;
            var task = GetProcessor("RU", "EUR1").ForRequest(request)
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                })
                                                .Catch((ArgumentNullException x) => anex = x)
                                                .Catch((DivideByZeroException x) => dbzex = x)
                                                .Finally(() => finalized = true);
            var _ = task.Result; // <- to wait for result or exception

            Assert.IsNull(anex, "anex");
            Assert.IsNotNull(dbzex, "dbzex");
            Assert.IsTrue(dbzex.Message == "hello from handler", "ex.ParamName == \"hello from handler\"");

            Assert.IsTrue(finalized, "finalized");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<AsyncVoid>), "typeof(Task<AsyncVoid>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _14_FinallyThrowsOnFaultResponseHandler()
        {
            var response = "";
            Exception dbzex = null;
            var finalized = false;
            var caught = false;
            var task = GetProcessor("RU", "EUR1").ForRequest("mock request")
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                    throw new DivideByZeroException("hello from handler");
                                                })
                                                .Finally(() => finalized = true);
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (AggregateException ex)
            {
                caught = true;
                dbzex = ex.Flatten().InnerException;
            }

            Assert.IsNotNull(dbzex, "dbzex");
            Assert.IsTrue(dbzex.Message == "hello from handler", "ex.Message == \"hello from handler\"");

            Assert.IsTrue(finalized, "finalized");
            Assert.IsTrue(caught, "caught");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<string>), "typeof(Task<string>)");
            Assert.IsTrue(task.Status == TaskStatus.Faulted, "task.Status == TaskStatus.Faulted");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _15_FinallyThrowsOnFaultRequest()
        {
            ArgumentNullException anex = null;
            var finalized = false;
            var caught = false;
            var task = GetProcessor("RU", "EUR1").ForRequest(Guid.Empty)
                                                .Finally(() => finalized = true);
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (AggregateException ex)
            {
                caught = true;
                anex = ex.Flatten().InnerException as ArgumentNullException;
            }

            Assert.IsNotNull(anex, "anex");
            Assert.IsTrue(anex.ParamName == "mock", "ex.ParamName == \"mock\"");

            Assert.IsTrue(finalized, "finalized");
            Assert.IsTrue(caught, "caught");

            Assert.IsNotNull(task, "task != null");
            //Assert.IsTrue(typeof(Task<string>).IsAssignableFrom(task.GetType()), "IsSubclassOf(typeof(Task<string>))");
            Assert.IsTrue(task.Status == TaskStatus.Faulted, "task.Status == TaskStatus.Faulted");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _16_FinallyHandlesSuccessfulRequest()
        {
            var response = "";
            Exception dbzex = null;
            var finalized = false;
            var caught = false;
            var task = GetProcessor("RU", "EUR1").ForRequest("mock request")
                                                .Finally(() => finalized = true);
            try
            {
                response = task.Result.As<string>(); // <- to wait for result or exception
            }
            catch (AggregateException ex)
            {
                caught = true;
                dbzex = ex.Flatten().InnerException;
            }

            Assert.IsNull(dbzex, "dbzex");

            Assert.IsTrue(finalized, "finalized");
            Assert.IsFalse(caught, "caught");

            Assert.IsNotNull(task, "task != null");
            //Assert.IsTrue(typeof(Task<string>).IsAssignableFrom(task.GetType()), "IsSubclassOf(typeof(Task<string>))");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'");
        }

        [TestMethod]
        [WorkItem(165592)]
        public void _17_FinallyHandlesSuccessfulResponse()
        {
            var response = "";
            Exception dbzex = null;
            var finalized = false;
            var caught = false;
            var task = GetProcessor("RU", "EUR1").ForRequest("mock request")
                                                .OnResponseOf<string>(s =>
                                                {
                                                    response = s;
                                                })
                                                .Finally(() => finalized = true);
            try
            {
                var _ = task.Result; // <- to wait for result or exception
            }
            catch (AggregateException ex)
            {
                caught = true;
                dbzex = ex.Flatten().InnerException;
            }

            Assert.IsNull(dbzex, "dbzex");

            Assert.IsTrue(finalized, "finalized");
            Assert.IsFalse(caught, "caught");

            Assert.IsNotNull(task, "task != null");
            Assert.IsInstanceOfType(task, typeof(Task<string>), "typeof(Task<string>)");
            Assert.IsTrue(task.Status == TaskStatus.RanToCompletion, "task.Status == TaskStatus.RanToCompletion");

            Assert.IsNotNull(response, "response != null");
            Assert.IsInstanceOfType(response, typeof(string), "typeof(string)");
            Assert.IsTrue(response == "mock", "response == 'mock'"); // <- set should be performed before the throwing.
        }

        //[Ignore]
        [TestMethod]
        [WorkItem(165592)]
        public void _18_AllContinuationsRunOnceAndOnTheSameWorkerThread()
        {
            var ids = new[] { 0, 0, 0, 0 };
            ids[0] = Thread.CurrentThread.GetHashCode();
            var response = "";
            var responsed = 0;
            var caught = 0;
            var finalized = 0;
            Exception actual = null;
            var task = GetProcessor("RU", "EUR1")
                .ForRequest("mock request")
                .OnResponseOf<string>(s =>
                {
                    responsed++;
                    response = s;
                    ids[1] = Thread.CurrentThread.GetHashCode();
                    throw new ArgumentNullException("mock");
                })
                .Catch((Exception ex) =>
                {
                    actual = ex;
                    caught++;
                    ids[2] = Thread.CurrentThread.GetHashCode();
                })
                .Finally(() =>
                {
                    finalized++;
                    ids[3] = Thread.CurrentThread.GetHashCode();
                });
            var ticks = 0;
            do
            {
                TestContext.WriteLine("tick:{0}", ticks++);
            } while (!task.Wait(30));


            Assert.IsNotNull(actual, "actual != null");
            Assert.IsInstanceOfType(actual, typeof(ArgumentNullException), "typeof(ArgumentNullException)");
            Assert.IsTrue((actual as ArgumentNullException).ParamName == "mock", "actual.Message == 'mock'");
            Assert.IsTrue(responsed == 1, "responsed");
            Assert.IsTrue(response == "mock", "response == 'mock'");
            Assert.IsTrue(caught == 1, "caught");
            Assert.IsTrue(finalized == 1, "finalized");
            Assert.IsTrue(ticks > 0, "ticks > 0");
            foreach (var id in Enumerable.Range(0, 4))
            {
                TestContext.WriteLine("ids[{0}]:{1}", id, ids[id]);
            }
            //Assert.IsTrue(ids[0] == ids[1], "ids[0] == ids[1]");
            Assert.IsTrue(ids[1] == ids[2], "ids[1] == ids[2]");
            Assert.IsTrue(ids[2] == ids[3], "ids[2] == ids[3]");
            Assert.IsTrue(th_id != ids[0], "th_id != ids[0]");
        }

        private int th_id = 0;
        private IMessageProcessor GetProcessor(string sub, string dep)
        {
            var mock = new Mock<IMessageProcessor>();

            mock.Setup(p => p.ProcessAsync(It.IsAny<IMessage>()))
                             .Returns(Task.Factory.StartNew(() =>
                             {
                                 Thread.Sleep(200);
                                 th_id = Thread.CurrentThread.GetHashCode();
                                 return (IMessage)Response<string>.New.WithBody("mock");
                             }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Current));
            mock.Setup(p => p.ProcessAsync(It.IsAny<Request<Guid>>()))
                             .Returns(() => Task.Factory.StartNew(() =>
                             {
                                 Thread.Sleep(200);
                                 th_id = Thread.CurrentThread.GetHashCode();
                                 throw new ArgumentNullException("mock");
                                 return default(IMessage);
                             }, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Current));

            IoC.RegisterInstance<IMessageProcessor>(mock.Object, dep);
            return ServiceBus.InContextOf(dep);
        }
    }
}
