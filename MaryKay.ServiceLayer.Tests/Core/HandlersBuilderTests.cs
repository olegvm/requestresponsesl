﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;
using MaryKay.ServiceLayer.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MaryKay.ServiceLayer.Tests.Core
{
    [TestClass]
    public class HandlersBuilderTests
    {
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void InitializeContainer(TestContext context)
        {
            var bs_count = Bootstrapper.Count;
        }

        [TestMethod]
        public void _01_BuilderHasBeenInstantiatedByBootstrapper()
        {
            var builder = IoC.Resolve<IHandlersChainBuilder>();

            Assert.IsNotNull(builder, "builder");
            var actual = builder as DefaultBuilder;
            Assert.IsNotNull(actual, "actual");
            Assert.IsTrue(actual.Handlers.Count == 5, "actual.Handlers.Count == 5");
        }

        [TestMethod]
        public void _02_BuilderStartsChainFromTheAppropriateHandler()
        {
            var builder = IoC.Resolve<IHandlersChainBuilder>() as DefaultBuilder;
            Assert.IsNotNull(builder, "builder");

            var start = builder.Build();
            var request = Request<string>.New;

            Assert.IsNotNull(request, "request");
            Assert.IsTrue(request.ID == Guid.Empty, "request.ID == Guid.Empty");
            Assert.IsTrue(request.Body == null, "request.Body == null");

            var response = start(request).Result;
            Assert.IsNotNull(response, "response");
            Assert.IsTrue(response.ID == Guid.Empty, "response.ID == Guid.Empty");
            Assert.IsTrue(response.State == MessageState.Rejected, "response.State == MessageState.Rejected");
            Assert.IsTrue(response.Body != null, "response.Body != null");

            var actual = response.As<string>();

            Assert.IsNotNull(actual, "actual");
            Assert.IsTrue(actual == "Unable to process a [null] request.", "actual == 'Unable to process a [null] request.'");
        }

        [TestMethod]
        public void _03_BuilderContinuesChainWithTheExceptionHandler()
        {
            var builder = IoC.Resolve<IHandlersChainBuilder>() as DefaultBuilder;
            Assert.IsNotNull(builder, "builder");

            var start = builder.Build();
            var guid = Guid.NewGuid();
            var request = Request<Guid>.New.WithID(guid).WithBody(guid);

            Assert.IsNotNull(request, "request");
            Assert.IsTrue(request.ID == guid, "request.ID == guid");
            Assert.IsTrue(request.Body != null, "request.Body != null");

            var response = start(request).Result;

            Assert.IsNotNull(response, "response");
            Assert.IsTrue(response.ID == guid, "response.ID == guid");
            Assert.IsTrue(response.State == MessageState.Failed, "response.State == MessageState.Failed");
            Assert.IsTrue(response.Body != null, "response.Body != null");
            Assert.IsTrue(response.Body is Exception, "response.Body is Exception");

            var actual = response.As<Exception>();

            Assert.IsTrue(actual is ApplicationException, "actual is ApplicationException");
            Assert.IsTrue(actual.InnerException == null, "actual.InnerException == null");
            Assert.IsTrue(actual.Message == string.Format("Can't resolve '{0}' implementation "
                    + "in the context of '{1}'.", typeof(Func<object, Task<object>>).FullName, typeof(Guid).FullName), "actual.Message is not matched");
        }

        [TestMethod]
        public void _04_BuilderEndsChainWithTheRequestProcessorHandler()
        {
            var builder = IoC.Resolve<IHandlersChainBuilder>() as DefaultBuilder;
            Assert.IsNotNull(builder, "builder");

            var start = builder.Build();
            var request = Request<EchoRequest>.Default.WithBody(new EchoRequest { Echo = "test", Delay = 342 });
            var guid = request.ID;

            Assert.IsNotNull(request, "request");
            Assert.IsTrue(request.ID == guid, "request.ID == guid");
            Assert.IsTrue(request.Body != null, "request.Body != null");
            Assert.IsTrue(request.Body is EchoRequest, "request.Body is EchoRequest");

            var response = start(request).Result;

            Assert.IsNotNull(response, "response");
            Assert.IsTrue(response.ID == guid, "response.ID == guid");
            Assert.IsTrue(response.State == MessageState.Processed, "response.State == MessageState.Processed");
            Assert.IsTrue(response.Body != null, "response.Body != null");
            Assert.IsTrue(response.Body is EchoRequest, "response.Body is EchoRequest");

            var actual = response.As<EchoRequest>();

            Assert.IsTrue(actual != null, "actual != null");
            var expected = string.Format("[test]:342 handled in:{0}ms", actual.Delay);
            if (actual.Echo != expected)
                TestContext.WriteLine(actual.Echo);
            Assert.IsTrue(actual.Delay >= 342, "actual.Delay >= 342");
            Assert.IsTrue(actual.Echo == expected, "actual.Echo is not matched");
        }

        [TestMethod]
        public void _05_BuilderContinuesChainWithTheCompositeHandler()
        {
            var dict = new Dictionary<string, int>
                {
                    {"test342", 342},
                    {"test242", 242},
                    {"test142", 142},
                    {"test42", 42},
                };
            var request = CompositeMessage.Default;
            var requests = dict.Select(p =>
                {
                    var req = Request<EchoRequest>.Default
                                                  .WithBody(new EchoRequest {Echo = p.Key, Delay = p.Value});
                    request.Add(req);
                    return new { req.ID, p.Value };
                }).ToDictionary(r => r.ID, r => r.Value);

            var guid = request.ID;

            Assert.IsNotNull(request, "request");
            Assert.IsTrue(request.ID == guid, "request.ID == guid");
            Assert.IsTrue(request.Body != null, "request.Body != null");
            Assert.IsTrue(request.Body is IEnumerable<IMessage>, "request.Body is IEnumerable<IMessage>");

            var start = IoC.Resolve<Func<IMessage, Task<IMessage>>>();
            var sw = new Stopwatch();
            sw.Start();
            var response = start(request).Result;
            sw.Stop();

            Assert.IsNotNull(response, "response");
            Assert.IsTrue(response.ID == guid, "response.ID == guid");
            Assert.IsTrue(response.State == MessageState.Processed, "response.State == MessageState.Processed");
            Assert.IsTrue(response.Body != null, "response.Body != null");
            Assert.IsTrue(response.IsComposite(), "response.IsComposite()");

            foreach (var resp in response.As<IEnumerable<IMessage>>())
            {
                Assert.IsTrue(resp != null, "resp != null");
                var delay = requests[resp.ID];
                var expected = string.Format("[test{0}]:{0} handled in:", delay);
                var actual = resp.As<EchoRequest>();
                Assert.IsTrue(actual != null, "actual != null");
                //Assert.IsTrue(actual.Delay >= delay, "actual.Delay >= delay");
                TestContext.WriteLine(actual.Echo);
                Assert.IsTrue(actual.Echo.StartsWith(expected), "actual.Echo.StartsWith(expected)");
            }

            var sum = requests.Sum(i => i.Value);
            TestContext.WriteLine(string.Format("CompositeMessage: {0}ms elapsed instead of {1}.", sw.ElapsedMilliseconds, sum));
            Assert.IsTrue(sw.ElapsedMilliseconds < (long)sum, "sw.ElapsedMilliseconds < sum");
        }

        [TestMethod]
        public void _00_Stub()
        {
        }
    }
}
