﻿using System;
using System.IO;
using System.Text;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Infrastructure;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace MaryKay.ServiceLayer.Tests.Components
{
    [TestClass]
    public class LoggerBaseTests
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        public void _01_CreateInstance()
        {
            var actual = new LoggerBase();

            Assert.IsNotNull(actual, "actual");
            Assert.IsInstanceOfType(actual, typeof(ILoggerFacade), "typeof(ILoggerFacade)");
        }

        [TestMethod]
        public void _02_CanUseMockedTextWriter()
        {
            var mock = new Mock<TextWriter>();
            var sb = new StringBuilder();
            mock.Setup(tw => tw.WriteLine(It.IsAny<string>())).Callback<string>(s => sb.AppendLine(s));

            AssertWriter(mock.Object, sb.ToString);
        }

        [TestMethod]
        public void _03_CanUseRealTextWriter()
        {
            using (var writer = new StringWriter())
            {
                AssertWriter(writer, writer.ToString);
            }
        }

        private async void AssertWriter(TextWriter writer, Func<string> getResult)
        {
            var logger = new LoggerBase().SetWriterAction(writer.WriteLineAsync)
                                         .SetVerbosityLevel(VerbosityLevel.Debug);

            await logger.WriteErrorAsync("mock");
            await logger.WriteVerboseAsync("{0}mock{1}", "mock", "mock");
            await logger.WriteDebugAsync(new DivideByZeroException("mock").ToSmoothString());

            var actual = getResult();
            Assert.IsNotNull(actual, "actual");
            Assert.IsTrue(actual.StartsWith("mock"), "actual.StartsWith(\"mock\")");
            Assert.IsTrue(actual.Contains("\r\nmockmockmock\r\n"), "actual.Contains(\"mockmockmock\")");
            Assert.IsTrue(actual.Contains("System.DivideByZeroException: mock"), "actual.Contains(\"System.DivideByZeroException: mock\")");
            TestContext.WriteLine(actual);
        }
    }
}
