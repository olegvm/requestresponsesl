using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;
using MaryKay.CRC.Contracts;
using MaryKay.CRC.Contracts.Entities.Imports;
using MaryKay.CRC.Contracts.Entities.TaskCenter;
using MaryKay.CRC.Contracts.ServiceContracts.Imports;
using MaryKay.ImportManagement.Services.Imports;
using MaryKay.ServiceLayer.Components;
using MaryKay.ServiceLayer.Core;
using MaryKay.ServiceLayer.Infrastructure;

namespace MaryKay.ServiceLayer.Services.ServiceAdapters
{
	public class GetImportCategoryRequest
	{
		public short ImportCategoryID { get; set; }
		public int SubsidiaryID { get; set; }
	}

	public class GetImportTypeRequest
	{
		public short ImportTypeID { get; set; }
		public int SubsidiaryID { get; set; }
	}

	public class IsReconcileRequest
	{
		public short ImportTypeID { get; set; }
		public DateTime ProcessMonth { get; set; }
		public int SubsidiaryID { get; set; }
	}

	public class GetImportCategoriesRequest
	{
		public int SubsidiaryID { get; set; }
	}

	public class GetImportTypesRequest
	{
		public int SubsidiaryID { get; set; }
		public short ImportCategoryID { get; set; }
	}

	public class CAMMaxMonthsAllowedRequest
	{
		public int SubsidiaryID { get; set; }
	}

	public class ValidateImportFileRequest
	{
		public ImportFile ImportFile { get; set; }
	}

	public class GetEnvServerPathRequest
	{
		public int SubsidiaryID { get; set; }
	}

	public class ProcessImportFileRequest
	{
		public ImportFile ImportFile { get; set; }
	}


    public class ImportServiceClientAdapter : IImportService
    {
		private Func<int, string> _getDep = (id) => 
		{
			var code = Enum.GetName(typeof(SubsidiaryCode), id);
			var depl = FrontOfficeDeployments.GetDeploymentForSub(code);
			return depl.ShortName;
		};
		public ImportCategory GetImportCategory(short importCategoryID, int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new GetImportCategoryRequest 
							  {
								 ImportCategoryID = importCategoryID,
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<ImportCategory>();
			}
        }

		public ImportType GetImportType(short importTypeID, int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new GetImportTypeRequest 
							  {
								 ImportTypeID = importTypeID,
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<ImportType>();
			}
        }

		public int IsReconcile(short importTypeID, DateTime processMonth, int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new IsReconcileRequest 
							  {
								 ImportTypeID = importTypeID,
								 ProcessMonth = processMonth,
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<int>();
			}
        }

		public List<ImportCategory> GetImportCategories(int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new GetImportCategoriesRequest 
							  {
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<List<ImportCategory>>();
			}
        }

		public List<ImportType> GetImportTypes(int subsidiaryID, short importCategoryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new GetImportTypesRequest 
							  {
								 SubsidiaryID = subsidiaryID,
								 ImportCategoryID = importCategoryID,
							  };
				return client.Process(Request.For(request)).As<List<ImportType>>();
			}
        }

		public int CAMMaxMonthsAllowed(int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new CAMMaxMonthsAllowedRequest 
							  {
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<int>();
			}
        }

		public List<string> ValidateImportFile(ImportFile importFile)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(importFile.SubsidiaryID)))
			{
				var request = new ValidateImportFileRequest 
							  {
								 ImportFile = importFile,
							  };
				return client.Process(Request.For(request)).As<List<string>>();
			}
        }

		public string GetEnvServerPath(int subsidiaryID)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(subsidiaryID)))
			{
				var request = new GetEnvServerPathRequest 
							  {
								 SubsidiaryID = subsidiaryID,
							  };
				return client.Process(Request.For(request)).As<string>();
			}
        }

		public List<ImportMessage> ProcessImportFile(ImportFile importFile)
        {
            using (var client = IoC.Resolve<IMessageProcessor>(_getDep(importFile.SubsidiaryID)))
			{
				var request = new ProcessImportFileRequest 
							  {
								 ImportFile = importFile,
							  };
				return client.Process(Request.For(request)).As<List<ImportMessage>>();
			}
        }

		public string SelfTest()
		{
			throw new NotImplementedException();
		}
    }

	public class ImportServiceHandlersAdapter : IHandlingService
	{
        public Task<object> HandleGetImportCategory(GetImportCategoryRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.GetImportCategory(request.ImportCategoryID, request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleGetImportType(GetImportTypeRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.GetImportType(request.ImportTypeID, request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleIsReconcile(IsReconcileRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.IsReconcile(request.ImportTypeID, request.ProcessMonth, request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleGetImportCategories(GetImportCategoriesRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.GetImportCategories(request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleGetImportTypes(GetImportTypesRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.GetImportTypes(request.SubsidiaryID, request.ImportCategoryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleCAMMaxMonthsAllowed(CAMMaxMonthsAllowedRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.CAMMaxMonthsAllowed(request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleValidateImportFile(ValidateImportFileRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.ValidateImportFile(request.ImportFile);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleGetEnvServerPath(GetEnvServerPathRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.GetEnvServerPath(request.SubsidiaryID);
            return Task.FromResult((object)result);
        }

        public Task<object> HandleProcessImportFile(ProcessImportFileRequest request)
        {
			var svc = IoC.Resolve<IImportService>();
			var result = svc.ProcessImportFile(request.ImportFile);
            return Task.FromResult((object)result);
        }

	}
}
