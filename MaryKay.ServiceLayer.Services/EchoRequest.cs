﻿namespace MaryKay.ServiceLayer.Services
{
    public class EchoRequest
    {
        public int Delay { get; set; }
        public string Echo { get; set; }
    }
}