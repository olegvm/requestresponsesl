﻿using System.Diagnostics;
using System.Threading.Tasks;
using MaryKay.ServiceLayer.Components;

namespace MaryKay.ServiceLayer.Services
{
    public class EchoService : IHandlingService
    {
        public async Task<object> HandleEchoRequest(EchoRequest request)
        {
            var sw = new Stopwatch();
            var str = string.Format("[{0}]:{1}", request.Echo, request.Delay);
            sw.Start();
            await Task.Delay(request.Delay);
            sw.Stop();

            str = string.Format("{0} handled in:{1}ms", str, sw.ElapsedMilliseconds);
            request.Delay = (int)sw.ElapsedMilliseconds;
            request.Echo = str;

            return request;
        }
    }
}