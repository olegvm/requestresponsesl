# Request Response Service Layer #

В проекте реализован обобщенный **Service Layer** на основе шаблона **RequestResponse** для использования в качестве **Anti Corruption Layer** при рефакторинге различных **WCF**-сервисов.

Также можно найти примеры использования шаблонов **Adapter**, **Builder**, **ChainOfResponsibility**, **ServiceBus**, **IoC**.